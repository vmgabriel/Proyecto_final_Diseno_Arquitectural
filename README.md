# Universidad Distrital Francisco José de Caldas
## Diseño arquitectural de software y patrones

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

### Miembros del grupo
- Karen Milena Pinilla Delgado - 20141020073
- Edwar Díaz Ruiz - 20141020004
- Daissi Bibiana Gonzalez Roldan - 20152020108
- Gabriel Vargas Monroy - 20141020107

### Grupo interno:
10

### Enunciado del problema

La figura siguiente, representa una estructura jerárquica tradicional que contiene información acerca de los departamentos de una compañía:

![Modelo mencionado por el profesor](image/modeloEmpresarial.png "Modelo Empresarial del enunciado")

- Para cada departamento el sistema contiene un número único de departamento, un valor de presupuesto y el número único del empleado que administra el departamento (El jefe).
- Para cada departamento, el sistema contiene la información acerca de todos los empleados que trabajan en el departamento, todos los proyectos cargados al departamento y todas las oficinas ocupadas por el departamento.
- La información de empleados se compone de un número único del empleado, el número del proyecto donde él o ella está trabajando y sus números de oficina y de teléfono.
- La información de los proyectos se compone de un número único del proyecto y un valor de presupuesto
- La información de las oficinas se compone de un número único de oficina y el área de esa oficina en metros cuadrados
- También para el empleado, el sistema contiene el nombre de cada cargo que el empleado ha desempeñado, junto con la fecha y el salario distinto recibido de ese cargo
- Para cada oficina contiene los números únicos de todos los teléfonos de esa oficina
- Para que se grabe un registro de cargo solo es generado porque un empleado lo ocupo
- El salario se define cuando un empleado ocupa un cargo
- Un proyecto lo realiza un solo departamento, según el problema, y un empleado trabaja en un solo departamento
- Solo hay un teléfono si existe una oficina, pero el teléfono para que exista no depende de que exista el empleado

Realizar una propuesta de arquitectura de negocio para este departamento, como unidad funcional

Realizar una propuesta de diseño arquitectural de software, para cubrir estos requerimientos de información

Realizar una propuesta de documentación de especificaciones para la problemática planteada

**Nota:** Las propuestas deben ser planteadas utilizando Archimate, como herramienta y lenguaje de descripción de arquitectura ADL y patrones de diseño arquitectural de software. Tenga en cuenta las recomendaciones planteadas por el Open Group

### Link Documento Final
[Documento Final](https://www.sharelatex.com/1686425665qjsvbfmmvkxp)

### Arquitectura Emprearial (ViewPoints)

<details>
<summary><h4>Capa de Negocio</h4></summary>
<details>
<summary><h5>Punto de Vista de Introductoria</h5></summary>
<img src="CapaNegocios/VistaIntroductoria/image/ViewPointIntro.png" >
</details>

<details>
<summary><h5>Punto de Vista de Organización</h5></summary>
<img src="CapaNegocios/VistaOrganizacion/image/ViewPointOrg.png" >
</details>

<details>
<summary><h5>Punto de Vista de Actor Cooperación</h5></summary>
<img src="CapaNegocios/VistaCooperacion/image/ViewPointActCoop.png" >
</details>

<details>
<summary><h5>Punto de Vista de Función de Negocio</h5></summary>
<img src="CapaNegocios/VistaFuncion/image/ViewPointFunct.png" >
</details>

<details>
<summary><h5>Punto de Vista de Procesos de Negocio</h5></summary>
<ul>
<li>Aprobar Proyecto<br>
<img src="CapaNegocios/VistaProceso/image/VPPAprobarProyecto.png" ></li>
<li> Ver Estado de Proyecto<br>
<img src="CapaNegocios/VistaProceso/image/VPPVerProyecto.png" ></li>
<li> Verificar Pago <br>
<img src="CapaNegocios/VistaProceso/image/VPPVerificarPago.png" ></li>
<li> Actualizar Registros a Proyecto <br>
<img src="CapaNegocios/VistaProceso/image/VPPActRegistros.png" ></li>
<li> Determinar Tiempo de Desarrollo <br>
<img src="CapaNegocios/VistaProceso/image/VPPTiempoDesarrollo.png" ></li>
<li> Actualizar Datos de Empleado <br>
<img src="CapaNegocios/VistaProceso/image/VPPActualizarDatos.png" ></li>
<li> Ver Informacion de Empleado <br>
<img src="CapaNegocios/VistaProceso/image/VPPVerEmpleado.png" ></li>
<li> Proceso de Contratación <br>
<img src="CapaNegocios/VistaProceso/image/VPPContratacion.png" ></li>
</ul>
</details>

<details>
<summary><h5>Punto de Vista de Cooperación de Proceso</h5></summary>
<ul>
<li>Aprobar Proyecto<br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpAprobar.png" ></li>
<li> Ver Estado de Proyecto<br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpestadoProyecto.png" ></li>
<li> Verificar Pago <br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpverificarPago.png" ></li>
<li> Actualizar Registros a Proyecto <br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpactualizarEstado.png" ></li>
<li> Determinar Tiempo de Desarrollo <br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncptiempoDesarrollo.png" ></li>
<li> Actualizar Datos de Empleado <br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpactualizarEstado.png" ></li>
<li> Ver Informacion de Empleado <br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpinformacionEmpleado.png" ></li>
<li> Proceso de Contratación <br>
<img src="CapaNegocios/VistaCooperacionProceso/image/cncpprocesoContratacion.png" ></li>
</ul>
</details>

<details>
<summary><h5>Punto de Vista de Producto</h5></summary>
<img src="CapaNegocios/VistaProducto/image/modelo.png" >
</details>

</details>


<details>
<summary><h4>Extension motivacional</h4></summary>
<ul>
<li>Modelo de Stakeholder<br>
<img src="Extension-motivacional/image/Stakeholder_1.png"></li>
<li> Modelo de Realizacion de objetivos<br>
<img src="Extension-motivacional/image/GoalRealization.png" ></li>
<li> Modelo de Contribucion de objetivos<br>
<img src="Extension-motivacional/image/GoalCooperation.png" ></li>
<li> Modelo de Principios<br>
<img src="Extension-motivacional/image/Principios.png" ></li>
<li> Modelo de Motivacion<br>
<img src="Extension-motivacional/image/vpmotivacion.png"></li>
</ul>
</details>


<details>
<summary><h4>Extension de implementacion y migracion</h4></summary>
<ul>
<li>Modelo de proyecto<br>
<img src="Extension-de-Implementacion/image/vpmProyecto.png" ></li>
<li> Modelo de migracion<br>
<img src="Extension-de-Implementacion/image/vpMigracion.png" ></li>
<li> Modelo de implementacion y migracion<br>
<img src="Extension-de-Implementacion/image/vpmImplementacion.png" ></li>
</ul>
</details>

<details>
<summary><h4>EModelo de datos</h4></summary>
<ul>
<li>Modelo Conceptual<br>
<img src="Modelo de Datos/image/ModeloConceptual.png" ></li>
<li> Modelo Fisico<br>
<img src="Modelo de Datos/image/ModeloFisico.png" ></li>
</ul>
</details>

---
