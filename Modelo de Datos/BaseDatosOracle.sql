/*==============================================================*/
/* DBMS name:      ORACLE Version 10gR2                         */
/* Created on:     12/07/2018 10:20:56 p. m.                    */
/*==============================================================*/


alter table CARGO
   drop constraint FK_CARGO_SALARIO_C_SALARIO;

alter table CARGO
   drop constraint FK_CARGO_TIENE_CAR_EMPLEADO;

alter table DEPARTAMENTO
   drop constraint FK_DEPARTAM_JEFE2_EMPLEADO;

alter table EMPLEADO
   drop constraint FK_EMPLEADO_EMPLEADO_DEPARTAM;

alter table EMPLEADO
   drop constraint FK_EMPLEADO_JEFE_DEPARTAM;

alter table EMPLEADO
   drop constraint FK_EMPLEADO_OFICINA_OFICINAS;

alter table EMPLEADO
   drop constraint FK_EMPLEADO_SUBALTERN_EMPLEADO;

alter table NUMEROS
   drop constraint FK_NUMEROS_TIENE_NUM_OFICINAS;

alter table OFICINAS
   drop constraint FK_OFICINAS_OCUPADAS_DEPARTAM;

alter table OFICINAS
   drop constraint FK_OFICINAS_OFICINA2_EMPLEADO;

alter table PRESUPUESTO
   drop constraint FK_PRESUPUE_HISTORICO_DEPARTAM;

alter table PRESUPUESTO_PROYECTO
   drop constraint FK_PRESUPUE_PRESUPUES_PROYECTO;

alter table PROYECTOS
   drop constraint FK_PROYECTO_ASIGNADO_DEPARTAM;

alter table PROYECTOS
   drop constraint FK_PROYECTO_EMP_PROYE_EMPLEADO;

drop index SALARIO_CARGO_FK;

drop index TIENE_CARGO_FK;

drop table CARGO cascade constraints;

drop index JEFE2_FK;

drop table DEPARTAMENTO cascade constraints;

drop index OFICINA_FK;

drop index SUBALTERNO_FK;

drop index JEFE_FK;

drop index EMPLEADO_FK;

drop table EMPLEADO cascade constraints;

drop index TIENE_NUMERO_FK;

drop table NUMEROS cascade constraints;

drop index OFICINA2_FK;

drop index OCUPADAS_FK;

drop table OFICINAS cascade constraints;

drop index HISTORICO_FK;

drop table PRESUPUESTO cascade constraints;

drop index PRESUPUESTO_PROYECTO_FK;

drop table PRESUPUESTO_PROYECTO cascade constraints;

drop index EMP_PROYECTO_FK;

drop index ASIGNADO_FK;

drop table PROYECTOS cascade constraints;

drop table SALARIO cascade constraints;

/*==============================================================*/
/* Table: CARGO                                                 */
/*==============================================================*/
create table CARGO  (
   ID_CARGO             VARCHAR2(2)                     not null,
   NOMBRE_CARGO         VARCHAR2(100)                   not null,
   ID_EMPLEADO          VARCHAR2(8),
   ID_SALARIO           VARCHAR2(5),
   constraint PK_CARGO primary key (ID_CARGO)
);

/*==============================================================*/
/* Index: TIENE_CARGO_FK                                        */
/*==============================================================*/
create index TIENE_CARGO_FK on CARGO (
   ID_EMPLEADO ASC
);

/*==============================================================*/
/* Index: SALARIO_CARGO_FK                                      */
/*==============================================================*/
create index SALARIO_CARGO_FK on CARGO (
   ID_SALARIO ASC
);

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO  (
   ID_DEPT              VARCHAR2(3)                     not null,
   NOMBRE_DEPT          VARCHAR2(150)                   not null,
   ID_EMPLEADO          VARCHAR2(8)                     not null,
   constraint PK_DEPARTAMENTO primary key (ID_DEPT)
);

/*==============================================================*/
/* Index: JEFE2_FK                                              */
/*==============================================================*/
create index JEFE2_FK on DEPARTAMENTO (
   ID_EMPLEADO ASC
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO  (
   ID_EMPLEADO          VARCHAR2(8)                     not null,
   NOMBRE_EMP           VARCHAR2(50)                    not null,
   APELLIDO             VARCHAR2(50),
   ID_DEPT              VARCHAR2(3),
   DEP_ID_DEPT          VARCHAR2(3),
   EMP_ID_EMPLEADO      VARCHAR2(8),
   ID_OFICINA           VARCHAR2(3),
   constraint PK_EMPLEADO primary key (ID_EMPLEADO)
);

/*==============================================================*/
/* Index: EMPLEADO_FK                                           */
/*==============================================================*/
create index EMPLEADO_FK on EMPLEADO (
   ID_DEPT ASC
);

/*==============================================================*/
/* Index: JEFE_FK                                               */
/*==============================================================*/
create index JEFE_FK on EMPLEADO (
   DEP_ID_DEPT ASC
);

/*==============================================================*/
/* Index: SUBALTERNO_FK                                         */
/*==============================================================*/
create index SUBALTERNO_FK on EMPLEADO (
   EMP_ID_EMPLEADO ASC
);

/*==============================================================*/
/* Index: OFICINA_FK                                            */
/*==============================================================*/
create index OFICINA_FK on EMPLEADO (
   ID_OFICINA ASC
);

/*==============================================================*/
/* Table: NUMEROS                                               */
/*==============================================================*/
create table NUMEROS  (
   ID_NUMERO            VARCHAR2(2)                     not null,
   TELEFONO             VARCHAR2(13)                    not null,
   ID_OFICINA           VARCHAR2(3),
   constraint PK_NUMEROS primary key (ID_NUMERO)
);

/*==============================================================*/
/* Index: TIENE_NUMERO_FK                                       */
/*==============================================================*/
create index TIENE_NUMERO_FK on NUMEROS (
   ID_OFICINA ASC
);

/*==============================================================*/
/* Table: OFICINAS                                              */
/*==============================================================*/
create table OFICINAS  (
   ID_OFICINA           VARCHAR2(3)                     not null,
   AREA                 NUMBER(5,2)                     not null,
   ID_DEPT              VARCHAR2(3),
   ID_EMPLEADO          VARCHAR2(8),
   constraint PK_OFICINAS primary key (ID_OFICINA)
);

/*==============================================================*/
/* Index: OCUPADAS_FK                                           */
/*==============================================================*/
create index OCUPADAS_FK on OFICINAS (
   ID_DEPT ASC
);

/*==============================================================*/
/* Index: OFICINA2_FK                                           */
/*==============================================================*/
create index OFICINA2_FK on OFICINAS (
   ID_EMPLEADO ASC
);

/*==============================================================*/
/* Table: PRESUPUESTO                                           */
/*==============================================================*/
create table PRESUPUESTO  (
   ID_PRESUPUESTO       VARCHAR2(6)                     not null,
   PRESUPUESTO          NUMBER(15,2)                    not null,
   FECHA                DATE                            not null,
   ID_DEPT              VARCHAR2(3),
   constraint PK_PRESUPUESTO primary key (ID_PRESUPUESTO)
);

/*==============================================================*/
/* Index: HISTORICO_FK                                          */
/*==============================================================*/
create index HISTORICO_FK on PRESUPUESTO (
   ID_DEPT ASC
);

/*==============================================================*/
/* Table: PRESUPUESTO_PROYECTO                                  */
/*==============================================================*/
create table PRESUPUESTO_PROYECTO  (
   ID_PRESUPUESTO2      VARCHAR2(6)                     not null,
   PRESUPUESTO          NUMBER(15,2)                    not null,
   FECHA                DATE                            not null,
   ID_PROYECTO          VARCHAR2(5),
   constraint PK_PRESUPUESTO_PROYECTO primary key (ID_PRESUPUESTO2)
);

/*==============================================================*/
/* Index: PRESUPUESTO_PROYECTO_FK                               */
/*==============================================================*/
create index PRESUPUESTO_PROYECTO_FK on PRESUPUESTO_PROYECTO (
   ID_PROYECTO ASC
);

/*==============================================================*/
/* Table: PROYECTOS                                             */
/*==============================================================*/
create table PROYECTOS  (
   ID_PROYECTO          VARCHAR2(5)                     not null,
   NOMBRE_PROYECTO      VARCHAR2(150)                   not null,
   ID_EMPLEADO          VARCHAR2(8),
   ID_DEPT              VARCHAR2(3),
   constraint PK_PROYECTOS primary key (ID_PROYECTO)
);

/*==============================================================*/
/* Index: ASIGNADO_FK                                           */
/*==============================================================*/
create index ASIGNADO_FK on PROYECTOS (
   ID_DEPT ASC
);

/*==============================================================*/
/* Index: EMP_PROYECTO_FK                                       */
/*==============================================================*/
create index EMP_PROYECTO_FK on PROYECTOS (
   ID_EMPLEADO ASC
);

/*==============================================================*/
/* Table: SALARIO                                               */
/*==============================================================*/
create table SALARIO  (
   ID_SALARIO           VARCHAR2(5)                     not null,
   SALARIO              NUMBER(12,2)                    not null,
   PAGADO               NUMBER(12,2),
   FECHA_SALARIO        DATE                            not null,
   FECHA_DE_PAGO        DATE,
   constraint PK_SALARIO primary key (ID_SALARIO)
);

alter table CARGO
   add constraint FK_CARGO_SALARIO_C_SALARIO foreign key (ID_SALARIO)
      references SALARIO (ID_SALARIO);

alter table CARGO
   add constraint FK_CARGO_TIENE_CAR_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO);

alter table DEPARTAMENTO
   add constraint FK_DEPARTAM_JEFE2_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO);

alter table EMPLEADO
   add constraint FK_EMPLEADO_EMPLEADO_DEPARTAM foreign key (ID_DEPT)
      references DEPARTAMENTO (ID_DEPT);

alter table EMPLEADO
   add constraint FK_EMPLEADO_JEFE_DEPARTAM foreign key (DEP_ID_DEPT)
      references DEPARTAMENTO (ID_DEPT);

alter table EMPLEADO
   add constraint FK_EMPLEADO_OFICINA_OFICINAS foreign key (ID_OFICINA)
      references OFICINAS (ID_OFICINA);

alter table EMPLEADO
   add constraint FK_EMPLEADO_SUBALTERN_EMPLEADO foreign key (EMP_ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO);

alter table NUMEROS
   add constraint FK_NUMEROS_TIENE_NUM_OFICINAS foreign key (ID_OFICINA)
      references OFICINAS (ID_OFICINA);

alter table OFICINAS
   add constraint FK_OFICINAS_OCUPADAS_DEPARTAM foreign key (ID_DEPT)
      references DEPARTAMENTO (ID_DEPT);

alter table OFICINAS
   add constraint FK_OFICINAS_OFICINA2_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO);

alter table PRESUPUESTO
   add constraint FK_PRESUPUE_HISTORICO_DEPARTAM foreign key (ID_DEPT)
      references DEPARTAMENTO (ID_DEPT);

alter table PRESUPUESTO_PROYECTO
   add constraint FK_PRESUPUE_PRESUPUES_PROYECTO foreign key (ID_PROYECTO)
      references PROYECTOS (ID_PROYECTO);

alter table PROYECTOS
   add constraint FK_PROYECTO_ASIGNADO_DEPARTAM foreign key (ID_DEPT)
      references DEPARTAMENTO (ID_DEPT);

alter table PROYECTOS
   add constraint FK_PROYECTO_EMP_PROYE_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADO (ID_EMPLEADO);

